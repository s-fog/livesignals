<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/flag.css',
        'css/style2.css',
        'css/site.css',
    ];
    public $js = [
        'js/js.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
