<?php

use common\models\Order;
use frontend\models\Payment;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('front', 'Account');
?>
<div class="row">
    <h1><?=Yii::t('translate', 'My account')?></h1>
</div>
<div class="user-registration">

    <?php $form = ActiveForm::begin([
        'id'=>'user',
        'layout'=>'horizontal',
        'validateOnBlur'=>false,
    ]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 50, 'autocomplete'=>'off', 'autofocus'=>true])->label('E-mail/Login') ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('front', 'Save'),
                ['class' => 'btn btn-primary']
            ) ?>
        </div>
    </div>

    <?php ActiveForm::end();?>

</div>
<br>
<?php if (Order::isActive()) {
    $order = Order::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
    ?>
    <div class="form-signin">
        <div class="alert alert-success text-center" role="alert">
            <div class="profile-value alert-link"><?=Yii::t('translate', 'Your subscription is active')?></div>
            <div>
                <?=Yii::t('translate', 'Expiration date')?>:
                <strong><?=gmdate('d.M.Y, H:i:s', $order->till)?></strong>
            </div>
        </div>
    </div>
<?php } ?>
<br>
<table class="st pricing-table" style="margin-top:1em;">
    <tbody><tr>
        <td colspan="3" class="text-center price-discount" style="padding:1em;background-color: #fcf8e3;">
            <div>
                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                <?=Yii::t('translate', 'Special offer')?>
                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
            </div>
        </td>
    </tr>
    <tr>
        <td class="price-discount">
            $29
        </td>
        <td class="price-discount" style="background-color:#EEFFEE;">
            $59
        </td>
        <td class="price-discount">
            $89
        </td>
    </tr>
    <tr>
        <td class="sd" style="text-align:center;">
            <div style="padding:1em 0.5em;font-size:150%;">1 <?=Yii::t('translate', 'months subscription')?></div>
            <div style="padding-bottom:1em;"><?=Yii::t('translate', 'All pairs')?></div>
        </td>
        <td class="sd" style="text-align:center;background-color:#EEFFEE;">
            <div style="padding:1em 0.5em;font-size:150%;">3 <?=Yii::t('translate', 'months subscription')?></div>
            <div style="padding-bottom:1em;"><?=Yii::t('translate', 'All pairs')?></div>
        </td>
        <td class="sd" style="text-align:center;">
            <div style="padding:1em 0.5em;font-size:150%;">6 <?=Yii::t('translate', 'months subscription')?></div>
            <div style="padding-bottom:1em;"><?=Yii::t('translate', 'All pairs')?></div>
        </td>
    </tr>
    <tr>
        <?php
        $payment = new Payment();
        $order_amount = '29';
        $sign = md5($payment->merchant_id.':'.$order_amount.':'.$payment->secret_word.':'.$payment->order_id);
        ?>
        <td class="sd" style="text-align:center;">
            <div style="margin:1em 0;">
                <form class="payment" action="http://www.free-kassa.ru/merchant/cash.php" method="get">
                    <input type='hidden' name='m' value='<?=$payment->merchant_id?>'>
                    <input type='hidden' name='oa' value='<?=$order_amount?>'>
                    <input type='hidden' name='o' value='<?=$payment->order_id?>'>
                    <input type='hidden' name='s' value='<?=$sign?>'>
                    <input type='hidden' name='lang' value='en'>
                    <input type='hidden' name='us_userid' value='<?=Yii::$app->user->identity->id?>'>
                    <input class="btn btn-primary btn-success "type="submit" value="<?=Yii::t('translate', 'Buy')?>"/>
                </form>
            </div>
        </td>
        <?php
        $order_amount = '59';
        ?>
        <td class="sd" style="text-align:center;background-color:#EEFFEE;">
            <div style="margin:1em 0;">
                <form class="payment" action="http://www.free-kassa.ru/merchant/cash.php" method="get">
                    <input type='hidden' name='m' value='<?=$payment->merchant_id?>'>
                    <input type='hidden' name='oa' value='<?=$order_amount?>'>
                    <input type='hidden' name='o' value='<?=$payment->order_id?>'>
                    <input type='hidden' name='s' value='<?=$sign?>'>
                    <input type='hidden' name='lang' value='en'>
                    <input type='hidden' name='us_userid' value='<?=Yii::$app->user->identity->id?>'>
                    <input class="btn btn-primary btn-success "type="submit" value="<?=Yii::t('translate', 'Buy')?>"/>
                </form>
            </div>
        </td>
        <?php
        $order_amount = '89';
        ?>
        <td class="sd" style="text-align:center;">
            <div style="margin:1em 0;">
                <form class="payment" action="http://www.free-kassa.ru/merchant/cash.php" method="get">
                    <input type='hidden' name='m' value='<?=$payment->merchant_id?>'>
                    <input type='hidden' name='oa' value='<?=$order_amount?>'>
                    <input type='hidden' name='o' value='<?=$payment->order_id?>'>
                    <input type='hidden' name='s' value='<?=$sign?>'>
                    <input type='hidden' name='lang' value='en'>
                    <input type='hidden' name='us_userid' value='<?=Yii::$app->user->identity->id?>'>
                    <input class="btn btn-primary btn-success "type="submit" value="<?=Yii::t('translate', 'Buy')?>"/>
                </form>
            </div>
        </td>
    </tr>
    </tbody></table>
<br>
<div class="form-signin alert alert-warning text-center" role="alert"><?=Yii::t('translate', 'Vat')?></div>
<br>
