<?php
use yii\helpers\Url;
?>
<div class="hidden-xs">
    <div>
        <a href="/" title="Free Forex Signals">
            <img src="/img/logo.jpg" alt="Livesignals.pro" class="img-responsive center-block" style="padding-top: 10px;">
        </a>
    </div>
    <nav class="navbar">
        <div class="text-center">
            <ul class="nav navbar-nav">
                <?php
                $languages = [
                    'en' => 'English',
                    'es' => 'Español',
                    'pt' => 'Português',
                    'fr' => 'Français',
                    'it' => 'Italiano',
                    'de' => 'Deutsch',
                    'ru' => 'Русский'
                ];
                $currentLanguageName = $languages[Yii::$app->language];
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle js-language"><span class="glyphicon glyphicon-flag"></span>&nbsp;<?=$currentLanguageName?><span class="caret"></span></a>
                    <ul class="dropdown-menu js-language-dropdown">
                        <?php
                        foreach($languages as $key => $value) {
                            if ($key == 'en') {
                                $iso = 'US';
                            } else {
                                $iso = strtoupper($key);
                            }

                            if (Yii::$app->controller->route == 'signal/view') {
                                echo '<li><a href="'.Url::to([
                                        '/'.Yii::$app->controller->route,
                                        'category_name' => $this->params['category_name'],
                                        'year' => $this->params['year'],
                                        'month' => $this->params['month'],
                                        'day' => $this->params['day'],
                                        'hourminute' => $this->params['hourminute'],
                                        'language' => $key
                                    ]).'"><span class="flag '.$iso.'"></span>&nbsp;'.$value.'</a></li>';
                            } else if (Yii::$app->controller->route == 'signal/month') {
                                echo '<li><a href="'.Url::to([
                                        '/'.Yii::$app->controller->route,
                                        'category_name' => $this->params['category_name'],
                                        'year' => $this->params['year'],
                                        'month' => $this->params['month'],
                                        'language' => $key
                                    ]).'"><span class="flag '.$iso.'"></span>&nbsp;'.$value.'</a></li>';
                            } else {
                                echo '<li><a href="'.Url::to(['/'.Yii::$app->controller->route, 'language' => $key]).'"><span class="flag '.$iso.'"></span>&nbsp;'.$value.'</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
                if (Yii::$app->user->isGuest) {
                    echo '<li>
                                <a href="'.Url::to(['/user-management/auth/registration']).'">
                                    <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                    '.Yii::t("translate", "Register").'
                                </a>
                            </li>';
                } else {
                    echo '<li>
                                <a href="'.Url::to(['/user/account']).'">
                                    <span class="glyphicon glyphicon-user"></span>
                                    '.Yii::t("translate", "My account").'
                                </a>
                            </li>';
                }
                ?>
                <?php
                if (Yii::$app->user->isGuest) {
                    echo '<li>
                                <a href="'.Url::to(['/user-management/auth/login']).'">
                                    <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                                    '.Yii::t("translate", "Login").'
                                </a>
                            </li>';
                } else {
                    echo '<li>
                                <a href="'.Url::to(['/user-management/auth/logout']).'">
                                    <span class="glyphicon glyphicon-log-in"></span>
                                    '.Yii::t("translate", "Logout").'
                                </a>
                            </li>';
                }
                ?>
            </ul>
        </div>
    </nav>
</div>