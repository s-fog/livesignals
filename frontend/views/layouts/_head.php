<?php
use yii\helpers\Html;
?>
<meta charset="<?=Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1000, maximum-scale=1.0, user-scalable=yes, initial-scale=1">
<?= Html::csrfMetaTags() ?>
<title>Free Forex signals &mdash; Livesignals.pro</title>
<link rel="icon" type="image/x-icon"
      href="https://forexcdn.appspot.com/fx3/img/favicon/alarm-bell-green.ico">
<?=$this->render('_metrics')?>
<?php $this->head() ?>