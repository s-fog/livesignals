<?php
/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

<?=$this->render('@frontend/views/layouts/_head')?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="container container-shadow">
    <?=$this->render('@frontend/views/layouts/_header')?>

    <?= $content ?>
</div>

<?php $this->endBody() ?>

<?php
$session = Yii::$app->session;
if ($session->isActive) {
    if ($session->get('register') == 'yes') { ?>
        <script>
            $(window).load(function() {
                yaCounter46314360.reachGoal('register');
                ga('send', 'event', 'ls', 'register');
                fbq('track', 'CompleteRegistration');
            });
        </script>
    <?php
        $session->remove('register');
    }

    if ($session->get('order') == 'yes') { ?>
        <script>
            $(window).load(function() {
                yaCounter46314360.reachGoal('order');
                ga('send', 'event', 'ls', 'order');
                fbq('track', 'Purchase');
            });
        </script>
        <?php
        $session->remove('order');
    }
}
?>
</body>
</html>
<?php $this->endPage() ?>
