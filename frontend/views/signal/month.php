<?php
use common\models\Signal;
use frontend\models\Locale;

$cn = str_replace('/', '', strtolower($category_name));

$this->params['category_name'] = $cn;
$this->params['year'] = $year;
$this->params['month'] = $month;
$summ = 0;
Yii::$app->formatter->locale = Locale::locale();
$M = Yii::$app->formatter->asDateTime(strtotime("01.$month.$year"), 'php:M')
?>
<div class="row">
    <h1><?=$h1?></h1>
</div>
<div class="row">
    <div id="month_graph">
        <img src="/<?=$imageName?>">
        <div class="month_graph_inner">
            <span class="first">3 <?=$M?></span>
            <span>10 <?=$M?></span>
            <span>17 <?=$M?></span>
            <span class="last">24 <?=$M?></span>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="st table table-hover">
        <tr>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Date, time')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Order')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Take Profit')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Stop Loss')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'State')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Entry')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Exit')?></th>
            <th class="sh" style="text-align:center;"><?=Yii::t('translate', 'Profit')?></th>
        </tr>
        <?php foreach($signals as $signal) {
            $dateTime = Yii::$app->formatter->asDateTime($signal['from'], 'php:D d M Y h:i:s A');
            $summ += $signal['profit_pips']*1;

            $class = '';
            if ($signal['condition'] == 'Cancelled') {
                $class = 'cancelled';
            } else {
                if ($signal['profit_pips']*1 > 0) {
                   $class = 'filled-profit';
                } else if ($signal['profit_pips']*1 < 0) {
                    $class = 'filled-loss';
                }
            }

            echo '
                <tr class="'.$class.'">
                    <td class="sd" style="text-align:center;">
                                <span>
                                    <small>
                                        <a style="" href="'.Signal::findOne($signal['id'])->url.'">
                                            							'.$dateTime.' UTC
                                        </a>
                                    </small>
                                </span>
                    </td>
                    <td class="sd" style="text-align:center;">'.Yii::t('translate', strtolower($signal['state'])).'</td>
                    <td class="sd" style="text-align:center;">'.$signal['take_profit_at'].'</td>
                    <td class="sd" style="text-align:center;">'.$signal['stop_loss_at'].'</td>
                    <td class="sd" style="text-align:center;;word-wrap: break-word;">'.Yii::t('translate', strtolower($signal['condition'])).'</td>';
            if ($signal['state'] == 'Buy') {
                echo '<td class="sd" style="text-align:center;">'.$signal['byu_at'].'</td>
                        <td class="sd" style="text-align:center;">'.$signal['sell_at'].'</td>';
            } else {
                echo '<td class="sd" style="text-align:center;">'.$signal['sell_at'].'</td>
                    <td class="sd" style="text-align:center;">'.$signal['byu_at'].'</td>';
            }

            echo '<td class="sd" style="text-align:center;">'.$signal['profit_pips'].'</td>
                </tr>';
        }

        echo '<tr class="filled-profit">
                    <td colspan="7" class="sd" style="text-align:left;padding-left:53px;">
                         <span style="font-weight:bold;">'.Yii::t('translate', 'Total').'</span>
                    </td>
                    <td class="sdplus" style="text-align:center;font-weight:bold;">'.$summ.'</td>
                </tr>';
        ?>
    </table>
</div>