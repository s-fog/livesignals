<?php
$cn = str_replace('/', '', strtolower($category_name));

$this->params['category_name'] = $cn;
$this->params['year'] = $year;
$this->params['month'] = $month;
$this->params['day'] = $day;
$this->params['hourminute'] = $hourminute;

?>
<div class="row">
    <h1><?=$signal->h1?></h1>
</div>
<br>
<br>
<div class="row">
    <div class="col-sm-4"></div>
    <?=$this->render('_item', [
        'signal' => $signal,
        'category_name' => $category_name
    ])?>
    <div class="col-sm-4"></div>
</div>
<br>
<br>
<br>
<div class="row">
    <h1><?=Yii::t('translate', 'instructions')?></h1>
</div>

<div class="text-regular"><?=Yii::t('translate', 'instructionsText1')?></div>
<div class="text-regular"><?=Yii::t('translate', 'instructionsText2')?></div>
