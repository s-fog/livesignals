<?php
use common\models\Order;
use frontend\models\Visit;
use yii\helpers\Url;

switch($category_name) {
    case 'EUR/USD': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/eur.jpg" alt="eur">&nbsp;
    <img src="/img/usd.jpg" alt="usd">
</div>';
        break;
    }
    case 'USD/CHF': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/usd.jpg" alt="usd">&nbsp;
    <img src="/img/chf.jpg" alt="chf">
</div>';
        break;
    }
    case 'GBP/USD': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/gbp.jpg" alt="gbp">&nbsp;
    <img src="/img/usd.jpg" alt="usd">
</div>';
        break;
    }
    case 'USD/JPY': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/usd.jpg" alt="usd">&nbsp;
    <img src="/img/jpy.jpg" alt="jpy">
</div>';
        break;
    }
    case 'USD/CAD': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/usd.jpg" alt="usd">&nbsp;
    <img src="/img/cad.jpg" alt="cad">
</div>';
        break;
    }
    case 'AUD/USD': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/aud.jpg" alt="aud">&nbsp;
    <img src="/img/usd.jpg" alt="usd">
</div>';
        break;
    }
    case 'EUR/JPY': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/eur.jpg" alt="eur">&nbsp;
    <img src="/img/jpy.jpg" alt="jpy">
</div>';
        break;
    }
    case 'NZD/USD': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/nzd.jpg" alt="nzd">&nbsp;
    <img src="/img/usd.jpg" alt="usd">
</div>';
        break;
    }
    case 'GBP/CHF': {
        $images = '<div class="col-sm-4" style="white-space:nowrap;">
    <img src="/img/gbp.jpg" alt="gbp">&nbsp;
    <img src="/img/chf.jpg" alt="chf">
</div>';
        break;
    }
}

$profitPips = str_replace('+', '', $signal->profit_pips);

$profitPips = '
<div class="col-sm-4 signal-value profit-color">
'.(($profitPips[0] == '-') ? $profitPips : "+".$profitPips).'
</div>';

$features = '';
$class = '';
$ss = '';

if($signal->condition == 'Filled') {
    $class = 'filled';
    $ss = 'Filled';
    if ($signal->state == 'Buy') {
        $features = '
                    <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "boughtAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->byu_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "soldAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->sell_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "profitPips").'</div>
							'.$profitPips.'
						</div>
					</div>
        ';
    } else if ($signal->state == 'Sell') {
        $features = '
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "soldAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->sell_at.'</div>
						</div>
					</div>
                    <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "boughtAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->byu_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "profitPips").'</div>
							'.$profitPips.'
						</div>
					</div>
        ';
    }

} else if($signal->condition == 'Cancelled') {
    $class = 'cancelled';
    $ss = 'Cancelled';
    if ($signal->state == 'Buy') {
		 $features = '
                    <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "boughtAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->byu_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "takeProfitAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->take_profit_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "stopLossAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->stop_loss_at.'</div>
						</div>
					</div>
        ';
    } else if ($signal->state == 'Sell') {
		 $features = '
               <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "soldAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->sell_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "takeProfitAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->take_profit_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "stopLossAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->stop_loss_at.'</div>
						</div>
					</div>
        ';
    }
} else {
    switch($signal->state) {
        case 'Buy': {
            $class = 'buy';
            $ss = 'Buy';
            $features = '
                    <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "buyAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->byu_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "takeProfitAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->take_profit_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "stopLossAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->stop_loss_at.'</div>
						</div>
					</div>
        ';
            break;
        }
        case 'Sell': {
            $class = 'sell';
            $ss = 'Sell';
            $features = '
                    <div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "sellAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->sell_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "takeProfitAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->take_profit_at.'</div>
						</div>
					</div>
					<div class="signal-item">
						<div class="row">
							<div class="col-sm-8">'.Yii::t("translate", "stopLossAt").'</div>
							<div class="col-sm-4 signal-value signal-color signal-price">'.$signal->stop_loss_at.'</div>
						</div>
					</div>
        ';
            break;
        }
    }
}

$timeLeft = '';
$timeDiff = time() - $signal->from;
if ($timeDiff < 86400) {
    $hours = intval($timeDiff / 3600);
    $minutes = intval($timeDiff / 3600);
}

$gmt = date('P', $signal->from);
$sign = $gmt[0];
$digit = ($gmt[1] . $gmt[2]) * 1;
$po = $sign . $digit . ' hours';

$from = strtotime($po, $signal->from);
$till = strtotime($po, $signal->till);

?>
<div class="col-xs-4 signal-cell">
    <div class="signal-border <?=(!$active && !Order::isActive())? "delay" : $class?>">
        <div class="signal_border_size">
            <div class="signal-header">
                <div class="row">
                    <?=$images?>
                    <div class="col-sm-8">
                        <a href="<?=$signal->url?>" style="text-decoration:none;" title="Free <?=$category_name?> Forex signal"><?=$category_name?></a>
                    </div>
                </div>
            </div>
            <?php if ($active || Order::isActive()) { ?>
                <div class="signal-body">
                    <div class="signal-item signal-ago1">
                        <div class="row">
                            <div class="col-sm-6"><?=$category_name?> <?=Yii::t('translate', 'signal')?></div>
                            <div class="col-sm-6 signal-color text-right">
                                <small><?=$timeLeft?></small>
                            </div>
                        </div>
                    </div>
                    <div class="signal-item">
                        <div class="row">
                            <div class="col-sm-4"><?=Yii::t('translate', 'from')?></div>
                            <div class="col-sm-8 signal-value signal-color">
                                <span class="gmt">GMT<?=date('P', $signal->from)?></span>
                                <?=date('H:i', $from);?>
                            </div>
                        </div>
                    </div>
                    <div class="signal-item">
                        <div class="row">
                            <div class="col-sm-4"><?=Yii::t('translate', 'till')?></div>
                            <div class="col-sm-8 signal-value signal-color">
                                <span class="gmt">GMT<?=date('P', $signal->till)?></span>
                                <?=date('H:i', $till);?>
                            </div>
                        </div>
                    </div>
                    <div class="signal-item signal-status signal-color text-center"><?=Yii::t('translate', strtolower($ss))?></div>
                    <?=$features?>
                    <?php if (!Order::isActive()) { ?>
                        <div class="signal-item">
                            <div class="row">
                                <div class="col-sm-8">
                                    <a href="<?=Url::to(['/user/account'])?>"><?=Yii::t('translate', 'Subscribe to signals')?> <?=$signal->category->name?></a>
                                </div>
                                <div class="col-sm-4  signal-value signal-color">
                                    <a href="<?=Url::to(['/user/account'])?>" title="<?=Yii::t('translate', 'Subscribe to signals')?> <?=$signal->category->name?>">
                                        <img alt="" style="border:0;margin-bottom:0;" src="https://forexcdn.appspot.com/fx3/img/signal/mail_sell.png">
                                        <img alt="" style="border:0;margin-bottom:0;" src="https://forexcdn.appspot.com/fx3/img/signal/mail_sell.png">
                                        <img alt="" style="border:0;margin-bottom:0;" src="https://forexcdn.appspot.com/fx3/img/signal/mail_sell.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="signal-body">
                    <div style="text-align:center;">
                        <div style="margin-top:0.5em;margin-bottom:0.5em;">
                            <img alt="" src="/img/bell72.png">
                        </div>
                        <div style="font-weight:bold;"><?=$category_name?> <?=Yii::t('translate', 'signal')?></div>
                        <div style="margin-top:0.5em;">
                            <a href="<?=Url::to(['/user-management/auth/login'])?>"><?=Yii::t('translate', 'Login')?></a>
                            &nbsp;
                            <a href="<?=Url::to(['/user-management/auth/registration'])?>"><?=Yii::t('translate', 'Register')?></a>
                        </div>
                        <div style="margin-top:1em;" class="alert alert-success">
                            <div>
                                <a class="alert-success" href="<?=Url::to(['/user/account'])?>">
                                    <?=Yii::t('translate', 'subscribe')?>
                                </a>
                            </div>
                            <div>
                                <em><?=Yii::t('translate', 'subscribeText')?></em>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
