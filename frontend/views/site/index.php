<?php

use common\models\Cookie;
use frontend\models\Totals;
use frontend\models\Visit;

$this->title = 'My Yii Application';
?>

<div class="row">
	<h1><?=Yii::t('translate', 'mainHeader')?></h1>
</div>

<div class="text-summary">
	<?=Yii::t('translate', 'topText')?>
</div>
<div id="signals">
    <?php
    $i = 0;
	$signalsIds = [];
    foreach($signals as $category => $signal) {
		$active = true;

		$ids = Cookie::find()->where(['ip' => $_SERVER['REMOTE_ADDR']])->one()->ids;
		if (empty($ids)) {
			$ids = $_COOKIE['google_visit'];
		}

		if (!empty($ids)) {
			foreach(json_decode($ids) as $key => $id) {
				if ($id == $signal->id && empty($signal->condition)) {
					if ($key > Visit::$count) {
						$active = false;
					}
				}
			}
		}

        if ($i % 3 == 0) {
            echo '<div class="row">';
        }

        echo $this->render('@frontend/views/signal/_item', [
            'category_name' => $category,
            'signal' => $signal,
			'active' => $active
        ]);

        if ($i % 3 == 2) {
            echo '</div>';
        }

        $i++;
		if (empty($signal->condition)) {
			$signalsIds[] = $signal->id;
		}
    }

	Visit::visit($signalsIds);
    ?>
</div>
<a id="performance"></a>
<div class="row">
    <h1><?=Yii::t('translate', 'currentPerformance')?></h1>
</div>
<div class="text-center">
    <img id="perf_img" class="img-responsive center-block" alt="Desempenho atual" src="/graphic/all.png">
    <div class="row">
		<span>
			<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/all.png';return false;" title="Desempenho de todos os sinais Forex">
				<?=Yii::t('translate', 'all')?>			</a>
		</span>
					<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/eurusd.png';return false;" title="EUR/USD Desempenho dos sinais Forex">
                    EUR/USD				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/usdchf.png';return false;" title="USD/CHF Desempenho dos sinais Forex">
                    USD/CHF				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/gbpusd.png';return false;" title="GBP/USD Desempenho dos sinais Forex">
                    GBP/USD				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/usdjpy.png';return false;" title="USD/JPY Desempenho dos sinais Forex">
                    USD/JPY				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/usdcad.png';return false;" title="USD/CAD Desempenho dos sinais Forex">
                    USD/CAD				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/audusd.png';return false;" title="AUD/USD Desempenho dos sinais Forex">
                    AUD/USD				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/eurjpy.png';return false;" title="EUR/JPY Desempenho dos sinais Forex">
                    EUR/JPY				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/nzdusd.png';return false;" title="NZD/USD Desempenho dos sinais Forex">
                    NZD/USD				</a>
			</span>
						<span>
				<a class="btn btn-default" href="#" role="button" onclick="document.getElementById('perf_img').src='/graphic/gbpchf.png';return false;" title="GBP/CHF Desempenho dos sinais Forex">
                    GBP/CHF				</a>
			</span>
    </div>
</div>
<br>
<br>
<div class="row">
	<h1><?=Yii::t('translate', 'instructions')?></h1>
</div>

<div class="text-regular"><?=Yii::t('translate', 'instructionsText1')?></div>
<div class="text-regular"><?=Yii::t('translate', 'instructionsText2')?></div>

<a id="totals"></a>
<div class="row">
	<h1>Totals</h1>
</div>
<div class="table-responsive">
	<table class="table table-hover table-condensedst">
		<tbody><tr>
			<th class="sh">Month, Year</th>
			<th class="sh">Total</th>
			<th class="sh">EURUSD</th>
			<th class="sh">USDCHF</th>
			<th class="sh">GBPUSD</th>
			<th class="sh">USDJPY</th>
			<th class="sh">USDCAD</th>
			<th class="sh">AUDUSD</th>
			<th class="sh">EURJPY</th>
			<th class="sh">NZDUSD</th>
			<th class="sh">GBPCHF</th>
		</tr>
		<?=$totals?>
		</tbody>
	</table>
</div>

<?php
if (!empty($currentUserOnline) || !empty($usersOnline)) { ?>
	<div class="row">
		<h1><?=Yii::t('translate', 'userOnline')?></h1>
	</div>
	<div class="row" style="margin-bottom:1em;">
		<?php if(!empty($currentUserOnline)) { ?>
			<div class="col-xs-6 col-md-4 col-lg-3 text-nowrap overflow-ellipsis">
				<span class="flag <?=$currentUserOnline->iso?>"></span>
				<?=$currentUserOnline->address?>
				<img src="/img/welcome.gif" alt="">
			</div>
		<?php } ?>

		<?php if (!empty($usersOnline)) { ?>
			<?php foreach($usersOnline as $useronline) { ?>
				<div class="col-xs-6 col-md-4 col-lg-3 text-nowrap overflow-ellipsis">
					<span class="flag <?=$useronline->iso?>"></span>
					<?=$useronline->address?>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
<?php } ?>

<div class="row">
	<h1><?=Yii::t('translate', 'disclaimer')?></h1>
</div>

<div class="small-disclaimer"><?=Yii::t('translate', 'disclaimerText1')?></div>
<div class="small-disclaimer"><?=Yii::t('translate', 'disclaimerText2')?></div>
<div class="small-disclaimer"><?=Yii::t('translate', 'disclaimerText3')?></div>
<div class="small-disclaimer"><?=Yii::t('translate', 'disclaimerText4')?></div>

<div class="text-center">
	<a href="mailto:info@Livesignals.pro.com">info@livesignals.pro</a>
	<br><a href="//www.free-kassa.ru/"><img src="//www.free-kassa.ru/img/fk_btn/13.png"></a>
</div>
<div class="text-center">
	&nbsp;
</div>
