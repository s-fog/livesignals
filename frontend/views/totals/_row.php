<?php
use yii\helpers\Url;
?>
<tr<?=($first) ? ' style="font-style:italic;"' : ''?>>
    <td class="sd"><?=$date?></td>
    <?php
    $total = 0;

    foreach($items as $item) {
        foreach($item as $ii) {
            $total = $total + $ii['profit_pips'];
        }
    }
    echo '<td class="sdtotal">'.$total.'</td>';
    if ($first) {
        foreach($items as $category_name => $item) {
            $res = 0;

            foreach($item as $ii) {
                $res = $res + $ii['profit_pips'];
            }

            echo '<td class="sdplus">'.$res.'</td>';
        }
    } else {
        foreach($items as $category_name => $item) {
            $res = 0;
            $link = Url::to([
                'signal/month',
                'category_name' => str_replace('/', '', strtolower($category_name)),
                'year' => date('Y', $timeStamp),
                'month' => date('m', $timeStamp)
            ]);

            foreach($item as $ii) {
                $res = $res + $ii['profit_pips'];
            }

            echo '<td class="sdplus"><a href="'.$link.'">'.$res.'</a></td>';
        }
    }
    ?>
</tr>