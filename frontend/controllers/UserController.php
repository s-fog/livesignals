<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class UserController extends Controller
{

    public function actions() {
        $this->layout = 'account';
    }

    public function actionAccount()
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user-management/auth/login']);
        }
        
        $user = Yii::$app->user->identity;

        if (!empty($_POST)) {
            $_POST['User']['email'] = $_POST['User']['username'];
            $user->load($_POST);
            $user->save();
        }

        return $this->render('account', ['model' => $user]);
    }
    
    public function actionOrder($month)
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['/user-management/auth/login']);
        }

        return $this->render('order');
    }
}
