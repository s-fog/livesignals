<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Parse;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class ParseController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        $model = new Parse;
        $model->page = '';

        while(empty($model->page)) {
            $model->page = $model->read('http://Livesignals.pro.com/en/login/login', 'http://livesignals.pro/source.html.txt', 's-fog@yandex.ru', 'Fog75864253$');
        }

        $model->page = str_replace('<sup>', '', $model->page);
        $model->page = str_replace('</sup>', '', $model->page);

        $model->createSignals($model);
    }

    public function actionImages()
    {
        $model = new Parse;
        $model->images();
    }

    public function actionFromfile()
    {
        $model = new Parse;
        $file = $_GET['file'];
        $fileHandle = fopen($file, "r");
        $model->fromFile($fileHandle);
    }
}
