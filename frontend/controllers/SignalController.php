<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Signal;
use frontend\models\Locale;
use frontend\models\Visit;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SignalController extends Controller
{
    public function actionView($category_name, $year, $month, $day, $hourminute)
    {
        $category_name = $category_name[0] . $category_name[1] . $category_name[2] . '/' . $category_name[3] . $category_name[4] . $category_name[5];
        $category_name = strtoupper($category_name);
        $category = Category::findOne(['name' => $category_name]);
        $from = strtotime("$year:$month:$day $hourminute[0]$hourminute[1]:$hourminute[2]$hourminute[3]:00");

        $signal = Signal::find()->where([
            'category_id' => $category->id,
            'from' => $from
        ])->one();

        return $this->render('view', [
            'signal' => $signal,
            'category_name' => $category_name,
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'hourminute' => $hourminute,
        ]);
    }
    
    public function actionMonth($category_name, $year, $month) {
        $startDate = strtotime("$year:$month:01 00:00:00");
        $toDate = strtotime('+1 month', $startDate) - 1;
        $categoryName = strtoupper($category_name);
        $categoryName = preg_replace('#([\w][\w][\w])([\w][\w][\w])#siU', '$1/$2', $categoryName);
        $category = Category::findOne(['name' => $categoryName]);

        $signals = Signal::find()
            ->where("signal.from > $startDate AND signal.from < $toDate")
            ->andWhere(['category_id' => $category->id])
            ->orderBy(['signal.from' => SORT_ASC])
            ->asArray()
            ->all();

        $a_data = array();
        $i = 0;
        foreach($signals as $a_signal)
        {
            if($i == 0)
                $a_data[$i] = $a_signal['profit_pips']*1;
            else
                $a_data[$i] = $a_data[$i-1]+$a_signal['profit_pips'];
            $i++;
        }

        $imageName = "graphic/months/$category_name-$month-$year.png";
        require "pChart/pData.class";
        require "pChart/pChart.class";
        $DataSet = new \pData(); // Создаём объект pData
        $DataSet->AddPoint($a_data, $category->name, "Category"); // Загружаем данные графика 1
        $DataSet->AddAllSeries(); // Добавить все данные для построения
        $Test = new \pChart(1000, 350); // Рисуем графическую плоскость
        $Test->setFontProperties("fonts/tahoma.ttf", 8); // Установка шрифта
        $Test->setGraphArea(50, 40, 950, 300); // Установка области графика
        $Test->drawFilledRoundedRectangle(7, 7, 693, 332, 5, 360, 360, 360); // Выделяем плоскость прямоугольником
        $Test->drawGraphArea(255, 255, 255, true); // Рисуем графическую плоскость
        $Test->drawScale($DataSet->GetData(), $DataSet->GetDataDescription(), SCALE_NORMAL, 0, 0, 0, true, 0, 2); // Рисуем оси и график
        $Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription()); // Соединяем точки графика линиями
        Yii::$app->formatter->locale = Locale::locale();
        $Test->drawTitle(400, 25, "$categoryName - ".Yii::$app->formatter->asDateTime($startDate, 'php:F Y'), 50, 50, 50, 585); // Выводим заголовок графика
        $Test->drawTitle(-520, 25, Yii::t('translate', 'Profit'), 50, 50, 50, 585);
        $Test->Render($imageName);

        return $this->render('month', [
            'h1' => Signal::monthH1($category_name, $year, $month),
            'signals' => $signals,
            'category_name' => $category_name,
            'year' => $year,
            'month' => $month,
            'imageName' => $imageName
        ]);
    }
}
