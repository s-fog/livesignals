<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Order;
use common\models\Signal;
use common\models\Useronline;
use DateInterval;
use DatePeriod;
use DateTime;
use frontend\models\Locale;
use frontend\models\Payment;
use frontend\models\Totals;
use frontend\models\Visit;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        Useronline::doDo();
        $signals = [];
        $categories = Category::find()->all();

        foreach($categories as $category) {
            $signal = Signal::find()->where(['category_id' => $category->id])->orderBy(['id' => SORT_DESC])->one();
            $signals[$category->name] = $signal;
        }

        $currentUserOnline = Useronline::find()->where(['ip' => $_SERVER['REMOTE_ADDR']])->one();

        if ($currentUserOnline) {
            $usersOnline = Useronline::find()->where(['!=', 'id', $currentUserOnline->id])->all();
        } else {
            $usersOnline = Useronline::find()->all();
        }
        

        //////////////////////////////////////////////////////////////////////////
                $totals = [];
        $categories = Category::find()->asArray()->all();

        $date = Yii::t('translate', 'Month-to-date');
        $first = $this->renderPartial('@frontend/views/totals/_row', [
            'items' => Totals::totals(time(), strtotime('-1 month', time()), $categories),
            'date' => $date,
            'first' => true
        ]);

        $time = time() - 3600*24*365*1.5;
        while ($time < time()) {
            $year = date('Y', $time);
            $month = date('m', $time);
            $from = strtotime("$year:$month:01 00:00:00");
            $to = strtotime('+1 month', $from) - 1;
            $time = $to + 2;
            Yii::$app->formatter->locale = Locale::locale();
            $date = Yii::$app->formatter->asDateTime($from, 'php:F Y');
            $totals[] = $this->renderPartial('@frontend/views/totals/_row', [
                'items' => Totals::totals($to, $from, $categories),
                'date' => $date,
                'timeStamp' => $from,
                'first' => false
            ]);
        }

        $totals = $first . implode('', array_reverse($totals));

        //////////////////////////////////////////////////////////////////////////
        if (isset($_GET['MERCHANT_ORDER_ID']) && isset($_GET['intid'])) {
            $session = Yii::$app->session;
            $session->set('order', 'yes');
        }
        //////////////////////////////////////////////////////////////////////////

        return $this->render('index', [
            'signals' => $signals,
            'currentUserOnline' => $currentUserOnline,
            'usersOnline' => $usersOnline,
            'totals' => $totals,
        ]);
    }

    public function actionError() {
        return $this->redirect(['site/index']);
    }

    public function actionCheckorder() {
        $payment = new Payment();
        $refererIp = 0;

        if(isset($_SERVER['HTTP_X_REAL_IP'])) {
            $refererIp = $_SERVER['HTTP_X_REAL_IP'];
        } else {
            $refererIp = $_SERVER['REMOTE_ADDR'];
        }

        if (in_array($refererIp, array('136.243.38.147', '136.243.38.149', '136.243.38.150', '136.243.38.151', '136.243.38.189', '88.198.88.98'))) {
            $sign = md5($payment->merchant_id.':'.$_REQUEST['AMOUNT'].':'.$payment->secret_word2.':'.$_REQUEST['MERCHANT_ORDER_ID']);

            if ($sign == $_REQUEST['SIGN']) {
                switch($_REQUEST['AMOUNT']) {
                    case '1': $months = 1;break;
                    case '59': $months = 3;break;
                    case '89': $months = 6;break;
                    default: $months = 0;
                }

                Order::addOrder($_REQUEST['us_userid']*1, $months, $_REQUEST['intid']*1);
                echo 'Success';
            } else {
                echo 'not podpis';
            }
        } else {
            echo 'ip wrong';
        }
    }
}
