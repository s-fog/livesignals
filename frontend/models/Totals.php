<?php

namespace frontend\models;

use common\models\Category;
use common\models\Signal;
use DateInterval;
use DatePeriod;
use DateTime;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Totals extends Model
{
    public static function totals($to, $from, $categories) {
        $result = [];

        foreach($categories as $category) {
            $result[$category['name']] = Signal::find()
                ->where([
                    'category_id' => $category['id']
                ])
                ->andWhere("signal.from > $from AND signal.from < $to")
                ->andWhere("`condition` != ''")
                ->asArray()->all();
        }

        return $result;
    }
}
