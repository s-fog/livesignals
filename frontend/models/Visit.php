<?php

namespace frontend\models;

use common\models\Cookie;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Visit extends Model
{
    public static $count = 9;

    public static function visit($ids) {
        if ((!isset($_COOKIE['google_visit']) && empty($_COOKIE['google_visit'])) || (!isset($_COOKIE['google_visit_till']) && empty($_COOKIE['google_visit_till']))) {
            Visit::start($ids);
        } else {
            if (!isset($_COOKIE['google_visit_till']) || $_COOKIE['google_visit_till']*1 < time()) {
                Visit::start($ids);
            } else {
                $cookieIds = json_decode($_COOKIE['google_visit']);

                foreach($ids as $id) {
                    if (!in_array($id, $cookieIds)) {
                        $cookieIds[] = $id;
                    }
                }

                setcookie("google_visit", json_encode($cookieIds), time() + 3600*24*30);
            }
        }

        $expireCookies = Cookie::find()->where('expire_date < ' . time())->all();

        foreach($expireCookies as $model) {
            $model->delete();
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $cookie = Cookie::find()->where(['ip' => $ip])->one();

        if ($cookie) {
            $cookieIds = json_decode($cookie->ids);

            foreach($ids as $id) {
                if (!in_array($id, $cookieIds)) {
                    $cookieIds[] = $id;
                }
            }

            $cookie->ids = json_encode($cookieIds);
            $cookie->save();
        } else {
            $cookie = new Cookie;
            $cookie->ids = json_encode($ids);
            $cookie->ip = $ip;
            $cookie->expire_date = time() + 3600*24;
            $cookie->save();
        }
    }

    public static function start($ids) {
        $time = time() + 3600*24;
        setcookie("google_visit_till", ''.$time.'', time() + 3600*24*30);
        setcookie("google_visit", json_encode($ids), time() + 3600*24*30);
    }
}
