<?php

namespace frontend\models;

use common\models\Order;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Payment extends Model
{
    public $merchant_id = 60427;
    public $secret_word = '3wtcr3km';
    public $secret_word2 = 'hsfy37kp';
    public $currency = 124;
    public $order_id = '';

    public function init()
    {
        parent::init();
        $ordersCount = Order::find()->where(['user_id' => Yii::$app->user->identity->id])->count();
        $this->order_id = Yii::$app->user->identity->email . '-' . ($ordersCount + 1);
    }
}
