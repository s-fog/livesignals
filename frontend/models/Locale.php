<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Locale extends Model
{
    public static function locale() {
        $locale = '';

        switch(Yii::$app->language) {
            case 'en': {
                $locale = 'en-US';
                break;
            }
            case 'es': {
                $locale = 'es-ES';
                break;
            }
            case 'pt': {
                $locale = 'pt-PT';
                break;
            }
            case 'fr': {
                $locale = 'fr-FR';
                break;
            }
            case 'it': {
                $locale = 'it-IT';
                break;
            }
            case 'de': {
                $locale = 'de-DE';
                break;
            }
            case 'ru': {
                $locale = 'ru-RU';
                break;
            }
        }

        return $locale;
    }

}
