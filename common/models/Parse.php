<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Parse extends Model
{
    public $z;
    public $zi;
    public $page;

   public function read($loginUrl, $url, $user_name, $user_password) {
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_HEADER, 1);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       //curl_setopt($ch, CURLOPT_PROXY, $ip);
       //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $loginpassw);
       // откуда пришли на эту страницу
       curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
       //запрещаем делать запрос с помощью POST и соответственно разрешаем с помощью GET
       curl_setopt($ch, CURLOPT_POST, 0);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
       //отсылаем серверу COOKIE полученные от него при авторизации
       //curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/cookie.txt');
       //curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

       $result = curl_exec($ch);

       curl_close($ch);

       if (strpos($result, 'recaptcha')) {

       }
       file_put_contents(Yii::getAlias('@www').'/recaptcha.html', $result);

       return $result;
   }
    
    public function showDTgmt($y, $m, $dd, $hh, $mm) {
        $timestamp = strtotime("$y:$m:$dd $hh:$mm:00");
        $gmt = date('O', $timestamp);
        $time = date('H:i', $timestamp + (($gmt[2]*1)*3600));
        $date = '<span class="gmt">GMT' . $gmt . '</span> ' . $time;
        return $date;
    }

    public function f($s) {
        $result = '';

        for ($i = 0; $i < strlen($s); $i++) {
            $result .= $this->z[ord($s[$i]) - $this->zi - $i];
        }

        return $result;
    }

    public function createSignals($model) {
        if (strpos($model->page, 'recaptcha') && file_exists($_SERVER['DOCUMENT_ROOT'].'/recaptcha.html')) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/recaptcha.html', $model->page);
        }

        preg_match_all('#<div class="col-xs-4 signal-cell">[\s]+<div class="[^\"]+">[\s]+<div class="[^\"]+">[\s]+<div class="[^\"]+">[\s]+<div class="row">[\s]+<div style="white-space:nowrap;" class="col-sm-4"><img[^>]+>[^<]+<img[^>]+></div>[\s]+<div class="col-sm-8">[\s]+<a[^>]+>([^<]+)</a>[\s]+</div>[\s]+</div>[\s]+</div>[\s]+<div class="signal-body">[\s]+<div class="[^\"]+">[\s]+<div class="row">[\s]+<div class="col-sm-6">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<small>([^<]+)</small>[\s]+</div>[\s]+</div>[\s]+</div>[\s]+<div class="signal-item">[\s]+<div class="row">[\s]+<div class="col-sm-4">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<span class="[^\"]+">[\s]+<script type="text/javascript">([^<]+)</script></span>[\s]+<script type="text/javascript">([^<]+)</script>[\s]+</div>[\s]+</div>[\s]+</div>[\s]+<div class="signal-item">[\s]+<div class="row">[\s]+<div class="col-sm-4">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<span class="[^\"]+">[\s]+<script type="text/javascript">([^<]+)</script></span>[\s]+<script type="text/javascript">([^<]+)</script>[\s]+</div>[\s]+</div>[\s]+</div>[\s]+<div class="[^\"]+">([^<]+)</div>[\s]+<div class="signal-item">[\s]+<div class="row">[\s]+<div class="col-sm-8">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<script type="text/javascript">([^<]+)</script></div>[\s]+</div>[\s]+</div>[\s]+<div class="signal-item">[\s]+<div class="row">[\s]+<div class="col-sm-8">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<script type="text/javascript">([^<]+)</script></div>[\s]+</div>[\s]+</div>[\s]+<div class="signal-item">[\s]+<div class="row">[\s]+<div class="col-sm-8">([^<]+)</div>[\s]+<div class="[^\"]+">[\s]+<script type="text/javascript">([^<]+)</script>[\s]*</div>[\s]+</div>[\s]+</div>#siU', $model->page, $matches);
        //var_dump($matches);die();
        unset($matches[0]);

        foreach($matches as $matchKey => $match) {
            foreach($match as $key => $value) {
                $matches[$matchKey][$key] = trim($value);
            }
        }

        $items = [];
        preg_match("#var z = '([^']+)';#siU", $model->page, $parseZ);
        preg_match("#zi=([^;]+);#siU", $model->page, $parseZi);
        $model->z = $parseZ[1];
        $model->zi = $parseZi[1];

        foreach($matches[1] as $key => $value) {
            $items[$value][0] = $matches[2][$key];
            $items[$value][1] = $matches[3][$key];

            $from = preg_match('#showDTgmt\((.*)\);#siU', $matches[5][$key], $fromMatch);
            $from = explode(',', $fromMatch[1]);
            $items[$value][3] = strtotime("$from[0]:$from[1]:$from[2] $from[3]:$from[4]:00");

            $items[$value][4] = $matches[7][$key];

            $till = preg_match('#showDTgmt\((.*)\);#siU', $matches[8][$key], $tillMatch);
            $till = explode(',', $tillMatch[1]);
            $items[$value][5] = strtotime("$till[0]:$till[1]:$till[2] $till[3]:$till[4]:00");

            $items[$value][6] = $matches[10][$key];
            $items[$value][7] = $matches[11][$key];

            $prop1 = preg_match('#f\(\'(.*)\'\);#siU', $matches[12][$key], $prop1Match);
            $prop1 = $model->f($prop1Match[1]);
            $items[$value][8] = $prop1;

            $items[$value][9] = $matches[13][$key];

            $prop2 = preg_match('#f\(\'(.*)\'\);#siU', $matches[14][$key], $prop2Match);
            $prop2 = $model->f($prop2Match[1]);
            $items[$value][10] = $prop2;

            $items[$value][11] = $matches[15][$key];

            $prop3 = preg_match('#f\(\'(.*)\'\);#siU', $matches[16][$key], $prop3Match);
            $prop3 = $model->f($prop3Match[1]);
            $items[$value][12] = $prop3;
        }

        $categories = Category::find()->all();
        foreach($categories as $category) {
            $signal = Signal::find()->where(['category_id' => $category->id])->orderBy(['id' => SORT_DESC])->one();

            if ($items[$category->name][6] == 'Filled') {
                $signal->category_id = $category->id;
                $signal->condition = $items[$category->name][6];
                $signal->from = $items[$category->name][3];
                $signal->till = $items[$category->name][5];
                $signal->byu_at = $items[$category->name][8];
                $signal->sell_at = $items[$category->name][10];
                $signal->profit_pips = $items[$category->name][12];
                $signal->save();
            } else if ($items[$category->name][6] == 'Cancelled') {
                $signal->category_id = $category->id;
                $signal->condition = $items[$category->name][6];
                $signal->from = $items[$category->name][3];
                $signal->till = $items[$category->name][5];
                $signal->sell_at = $items[$category->name][8];
                $signal->take_profit_at = $items[$category->name][10];
                $signal->stop_loss_at = $items[$category->name][12];
                $signal->save();
            } else if ($items[$category->name][6] == 'Sell') {
                if ($items[$category->name][6] != $signal->state) {
                    $signal = new Signal;
                }

                $signal->category_id = $category->id;
                $signal->state = $items[$category->name][6];
                $signal->condition = '';
                $signal->from = $items[$category->name][3];
                $signal->till = $items[$category->name][5];
                $signal->sell_at = $items[$category->name][8];
                $signal->take_profit_at = $items[$category->name][10];
                $signal->stop_loss_at = $items[$category->name][12];
                $signal->save();
            } else if ($items[$category->name][6] == 'Buy') {
                if ($items[$category->name][6] != $signal->state) {
                    $signal = new Signal;
                }

                $signal->category_id = $category->id;
                $signal->state = $items[$category->name][6];
                $signal->condition = '';
                $signal->from = $items[$category->name][3];
                $signal->till = $items[$category->name][5];
                $signal->byu_at = $items[$category->name][8];
                $signal->take_profit_at = $items[$category->name][10];
                $signal->stop_loss_at = $items[$category->name][12];
                $signal->save();
            }
        }
    }

    public function images() {
        $images = [
            '/img/pips/all',
            '/img/pips/eurusd',
            '/img/pips/usdchf',
            '/img/pips/gbpusd',
            '/img/pips/usdjpy',
            '/img/pips/usdcad',
            '/img/pips/audusd',
            '/img/pips/eurjpy',
            '/img/pips/nzdusd',
            '/img/pips/gbpchf',
        ];

        foreach($images as $image) {
            preg_match('#/.*/.*/(.*)$#siU', $image, $match);
            $filename = $match[1] . '.png';
            $msg = time()." -> ".copy('http://foresignal.com'.$image, Yii::getAlias('@www').'/graphic/'.$filename) . " -> http://foresignal.com$image -> $filename\r\n";
            file_put_contents(Yii::getAlias('@www') . '/logImages.txt', $msg, FILE_APPEND | LOCK_EX);
        }

        file_put_contents(Yii::getAlias('@www') . '/logImages.txt', "\r\n", FILE_APPEND | LOCK_EX);
    }
    
    public function fromFile($fileHandle) {
        while (($data = fgetcsv($fileHandle, 100000, ";")) !== FALSE) {
            $categoryName = strtoupper($data[0]);
            $categoryName = preg_replace('#([\w][\w][\w])([\w][\w][\w])#siU', '$1/$2', $categoryName);
            $category = Category::findOne(['name' => $categoryName]);

            $from = strtotime("$data[1]:$data[2]:$data[3] $data[4]:$data[5]:00");
            $till = strtotime('+4 hours', $from);

            $state = '';
            $buy_at = '';
            $sell_at = '';
            if ($data[6] == 'b') {
                $state = 'Buy';
                $buy_at = $data[7];
                $sell_at = $data[8];
            } else if ($data[6] == 's') {
                $state = 'Sell';
                $buy_at = $data[8];
                $sell_at = $data[7];
            }

            $condition = '';
            if ($data[11] == 'i') {
                $condition = 'Filled';
            } else if ($data[11] == 'a') {
                $condition = 'Cancelled';
            }

            $signal = Signal::find()
                ->where(['category_id' => $category->id])
                ->andWhere(['from' => $from])
                ->andWhere(['till' => $till])
                ->one();
            if (!$signal) {
                $signal = new Signal();
                $signal->category_id = $category->id;
                $signal->from = $from;
                $signal->till = $till;
                $signal->state = $state;
                $signal->condition = $condition;
                $signal->byu_at = $buy_at;
                $signal->sell_at = $sell_at;
                $signal->take_profit_at = $data[9];
                $signal->stop_loss_at = $data[10];
                $signal->profit_pips = $data[12];
                var_dump($signal->save());
            } else {
                var_dump(' alredy here ');
            }
        }
    }
}
