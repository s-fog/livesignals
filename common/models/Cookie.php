<?php

namespace common\models;

use Yii;
use \common\models\base\Cookie as BaseCookie;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cookie".
 */
class Cookie extends BaseCookie
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }
}
