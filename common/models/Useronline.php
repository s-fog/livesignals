<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "useronline".
 *
 * @property integer $id
 * @property string $session_id
 * @property integer $time
 * @property string $ip
 * @property string $address
 * @property string $iso
 */
class Useronline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'useronline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'integer'],
            [['session_id', 'address'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 100],
            [['iso'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_id' => 'Session ID',
            'time' => 'Time',
            'ip' => 'Ip',
            'address' => 'Address',
            'iso' => 'Iso',
        ];
    }

    public static function doDo() {
        $session_id = session_id();
        $time = time();
        $time_check = $time + 1800;
        $ip = $_SERVER['REMOTE_ADDR'];
        $address = Yii::$app->sypexGeo->getCityFull($ip);

        $userOnline = Useronline::find()->where(['ip' => $ip])->one();

        if ($userOnline) {
            $userOnline->time = $time_check;
            $userOnline->save();
        } else if (!$userOnline && !empty($address['city']['name_en']) && !empty($address['country']['name_en'])) {
            $userOnline = new Useronline();
            $userOnline->session_id = $session_id;
            $userOnline->time = $time_check;
            $userOnline->ip = $ip;
            $userOnline->iso = $address['country']['iso'];
            $userOnline->address = $address['country']['name_en'] . ', ' . $address['city']['name_en'];
            $userOnline->save();
        }

        foreach(Useronline::find()->where('time < ' . time())->all() as $user) {
            $user->delete();
        }
    }
}
