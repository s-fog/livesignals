<?php

namespace common\models;

use Yii;
use \common\models\base\Signal as BaseSignal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "signal".
 */
class Signal extends BaseSignal
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                  # custom validation rules
             ]
        );
    }

    public static function fromTill($str, $created_at) {
        $year = date('Y', $created_at);
        $month = date('m', $created_at);
        $day = date('d', $created_at);
        preg_match('#([0-9]+):([0-9]+)$#siU', $str, $match);
        $hour = $match[1]*1;

        if ($hour == 0) {
            $hour = 21;
        } else if ($hour == 1) {
            $hour = 22;
        } else if ($hour == 2) {
            $hour = 23;
        } else {
            $hour = $hour - 3;
        }

        if (strlen($hour) == 1) {
            $hour = '0'.$hour;
        }

        $minute = $match[2];
        return strtotime("$year:$month:$day $hour:$minute:00");
    }

    public function getUrl() {
        $category_name = str_replace('/', '', $this->category->name);
        $category_name = strtolower($category_name);

        return Url::to(['signal/view',
            'category_name' => $category_name,
            'year' => date('Y', $this->from),
            'month' => date('m', $this->from),
            'day' => date('d', $this->from),
            'hourminute' => date('H', $this->from) . date('i', $this->from)
        ]);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getH1()
    {
        $category_name = $this->category->name;
        $weekDay = date('D', $this->from);
        $day = date('d', $this->from);
        $month = date('M', $this->from);
        $year = date('Y', $this->from);
        $time = date('h:i:s', $this->from);
        $amPm = date('A', $this->from);

        return "$category_name Forex ".Yii::t('translate', 'signal')." @ $weekDay $day $month $year $time $amPm UTC";
    }

    public static function monthH1($category_name, $year, $month)
    {
        $category_name = $category_name[0] . $category_name[1] . $category_name[2] . '/' . $category_name[3] . $category_name[4] . $category_name[5];
        $category_name = strtoupper($category_name);
        $date = strtotime("01.$month.$year");

        return "$category_name Forex ".Yii::t('translate', 'signals').", ".date('F', $date)." $year";
    }
}
