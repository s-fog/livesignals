<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Signal;

/**
* SignalSearch represents the model behind the search form about `common\models\Signal`.
*/
class SignalSearch extends Signal
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'category_id', 'created_at', 'updated_at'], 'integer'],
            [['from', 'till', 'state', 'byu_at', 'take_profit_at', 'stop_loss_at', 'sell_at', 'profit_pips'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Signal::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'from', $this->from])
            ->andFilterWhere(['like', 'till', $this->till])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'byu_at', $this->byu_at])
            ->andFilterWhere(['like', 'take_profit_at', $this->take_profit_at])
            ->andFilterWhere(['like', 'stop_loss_at', $this->stop_loss_at])
            ->andFilterWhere(['like', 'sell_at', $this->sell_at])
            ->andFilterWhere(['like', 'profit_pips', $this->profit_pips]);

return $dataProvider;
}
}