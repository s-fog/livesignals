<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $till
 * @property integer $intid
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'till'], 'required'],
            [['user_id', 'till', 'intid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'till' => 'Till',
        ];
    }

    public static function addOrder ($user_id, $monthsCount, $intid) {
        $oldOrder = Order::find()->where([
            'user_id' => $user_id
        ])->andWhere('till > '.time())->one();

        var_dump($user_id);
        var_dump(($oldOrder->till - time()) + $monthsCount * 3600*24*30);
        var_dump($intid);

        if ($oldOrder) {
            $order = new Order();
            $order->user_id = $user_id;
            $order->till = time() + ($oldOrder->till - time()) + $monthsCount * 3600*24*30;
            $order->intid = $intid;
            return $order->save();
        } else {
            $order = new Order();
            $order->user_id = $user_id;
            $order->till = time() + $monthsCount * 3600*24*30;
            $order->intid = $intid;
            return $order->save();
        }
    }

    public static function isActive () {
        if (!Yii::$app->user->isGuest) {
            $order = Order::find()->where(['user_id' => Yii::$app->user->identity->id])->one();

            if ($order) {
                if ($order->till > time()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
