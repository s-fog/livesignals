<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'Registrarse',
    'My account' => 'Mi Cuenta',
    'Login' => 'Inicio de sesión',
    'Logout' => 'Desconectar',
    'mainHeader' => 'Señales Forex gratis',
    'topText' => '
        Livesignals.pro proporciona 
        <strong>señales de Forex gratis</strong>
         en línea con 
        <a href="#performance">resultados</a>
         y 
         <a href="#totals">totales mensuales</a>
         en tiempo real.
        <br>
        Para mantenerse informado, 
        <a href="'.Url::to(["site/index"]).'">actualice esta página</a>
        o  
        <a href="'.Url::to(["user/account"]).'">suscríbase por correo electrónico</a>
    ',
    'userOnline' => 'Usuarios online',
    'instructions' => 'Instrucciones',
    'instructionsText1' => 'El pedido pendiente tiene que hacerse cuando llegue la senal (hora "Desde"). La hora "Hasta" esta hecha para forzar la salida. Cualquier cambio abierto esta "Realizado" cuando la hora "Hasta" este a punto de llegar. Cualquier pedido pendiente esta "Cancelado" cuando la hora "Hasta" este a punto de llegar. Usar trailing-stop para maximizar ganancias.',
    'instructionsText2' => 'Por favor, tenga en cuenta que distintos agentes de bolsa ofrecen distintas cotizaciones en el mismo momento. La diferencia es normalmente unos 5 pips o quizas mas. Para resolver este problema Livesignals.pro trata de hacer un promedio de precios de distintos agentes de bolsa y ofrece resultados medios. Sin embargo, es posible que su cambio alcance el nivel de entry/take-profit/stop-loss a diferencia del cambio de Livesignals.pro y viceversa debido a la diferencia de precios.',
    'disclaimer' => 'Descargo de responsabilidad',
    'disclaimerText1' => 'La operación de Futuros y Opciones tiene potencial de importantes ganancias, pero también potencial de un importante riesgo. Debes estar prevenido de los riesgos y estar dispuesto a aceptarlos cuando inviertas en mercados de futuros y opciones. No operes con dinero que no puedas permitirte perder. Esta no es ni una solicitud ni una oferta de comprar/vender futuros u opciones. Ninguna representación esta siendo hecha de que cualquier cuenta alcanzara o probablemente alcanzara ganancias o perdidas similares a aquellas tratadas en este sitio web. El desempeño pasado de cualquier sistema o metodologia de operación no es necesariamente indicativo de futuros resultados.',
    'disclaimerText2' => 'REGLA CFTC 4.41 - LOS RESULTADOS DE DESEMPEÑO HIPOTETICOS O SIMULADOS TIENEN CIERTAS LIMITACIONES. A DIFERENCIA DE UN REGISTRO REAL DE DESEMPEÑO, LOS RESULTADOS SIMULADOS NO REPRESENTAN OPERACIONES REALES. TAMBIEN, DADO QUE LAS OPERACIONES NO HAN SIDO EJECUTADAS LOS RESULTADOS PUEDEN SER SUB-O-SOBRE COMPENSADOS POR EL IMPACTO, SI LO HAY, DE CIERTOS FACTORES DE MERCADO TALES COMO FALTA DE LIQUIDEZ. LOS PROGRAMAS DE OPERACION SIMULADA EN GENERAL ESTAN TAMBIEN SUJETOS AL HECHO DE QUE FUERON DISEÑADOS CON EL BENEFICIO DE LA RETROSPECTIVA. NINGUNA REPRESENTACION ESTA SIENDO HECHA DE QUE CUALQUIER CUENTA ALCANZARA O PROBABLEMENTE ALCANZARA GANANCIAS O PERDIDAS SIMILARES A AQUELLAS MOSTRADAS.',
    'disclaimerText3' => 'Por la presente, los propietarios del sitio web y el sitio web no se responsabilizan del uso del sitio web y/o de la información. El uso del sitio web, el contenido y la información son sólo responsabilidades del usuario. Por la presente, el usuario libera a los propietarios del sitio web de cualquier responsabilidad por daños ocasionados en el equipo, en caso de haberlos, a través del uso del sitio web, su contenido y/o sus diversos servicios. Por la presente, el usuario libera a los propietarios del sitio web, al sitio web, a los socios, agentes, empleados, directivos, gerentes, directores, accionistas, etc. de cualquier responsabilidad por pérdidas, daños financieros, daños personales físicos y/o fraude ocasionados por el uso del sitio web.',
    'disclaimerText4' => 'Dodos los derechos reservados. El hecho de utilizar esta página significa que acepta la licencia de usuario.',
    'currentPerformance' => 'Resultados actuales',
    'all' => 'TODOS',
    'Month-to-date' => 'Los últimos 30 días',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Compra',
    'sell' => 'Venta',
    'filled' => 'Realizado',
    'cancelled' => 'Cancelado',
    'from' => 'desde',
    'till' => 'hasta',
    'soldAt' => 'Vendido por',
    'boughtAt' => 'Comprado por',
    'profitPips' => 'Ganancias, puntos',
    'buyAt' => 'Compre por',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Venda por',
    'subscribe' => 'Suscribirse a las señales',
    'subscribeText' => 'Los suscriptores tienen acceso instantáneo a las señales.',
    'Subscribe to signals' => 'Subscríbase a las señales de',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => 'Oferta Especial',
    'months subscription' => 'Subscripción de meses',
    'All pairs' => 'Todos los pares',
    'Buy' => 'Comprar',
    'Vat' => 'IVA se puede aplicar a la cantidad que tiene que pagar. IVA significa Impuesto al Valor Agregado, y es un impuesto añadido por el gobierno para el precio de compra.',
    'Your subscription is active' => 'La suscripción está activo',
    'Expiration date' => 'Fecha de Expiración',
    'Save' => 'Guardar',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => 'Dirección de correo electrónico (a la dirección real)',
    'Password (6+ characters)' => 'Contraseña (6+ caracteres)',
    'Repeat your password' => 'Repetir contraseña',
    'Register a new account' => 'Registrar una nueva cuenta',
    'Please enter above characters' => 'Por favor, ingrese los caracteres anteriores',
    '' => '',
    'Please login' => 'Por Favor Inicia Sesión',
    'Email' => '',
    'Password' => 'Contraseña',
    'Remember me?' => 'Recuérdame durante',
    'Forgot password ?' => 'He olvidado mi contraseña',
    ///////Auth END
    ///////Month
    'Date, time' => 'Fecha, hora',
    'Order' => 'Orden',
    'Take Profit' => '',
    'Stop Loss' => '',
    'State' => 'Estado',
    'Entry' => 'Entrada',
    'Exit' => 'Salida',
    'Profit' => 'Ganancias',
    ///////Month END
];
?>