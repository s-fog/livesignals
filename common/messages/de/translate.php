<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'Registrieren',
    'My account' => 'Mein Konto',
    'Login' => 'Anmelden',
    'Logout' => 'Abmelden',
    'mainHeader' => 'Kostenlose Forex-Signale',
    'topText' => '
        Livesignals.pro bietet 
        <strong>kostenlose Forex signale online</strong>
         mit 
        <a href="#performance">Echtzeit-Ergebnissen</a>
         und 
         <a href="#totals">Bilanzen</a>.
        <br>
        Für aktuelle Informationen
        <a href="'.Url::to(["site/index"]).'">laden Sie diese Seite neu</a>
        oder 
        <a href="'.Url::to(["user/account"]).'">abonnieren Sie per E-Mail</a>.
    ',
    'userOnline' => 'Benutzer online',
    'instructions' => 'Anleitung',
    'instructionsText1' => 'Eine anstehende Order soll beim Empfangen des Signals (zu der "Von"-Zeit) erteilt werden. Die "Bis"-Zeit ist fur erzwungenes Beenden bestimmt. Beliebige offene Handelsaktion gilt als erledigt, wenn die "Bis"-Zeit beinahe erreicht ist. Beliebige anstehende Order wird abgebrochen, wenn die "Bis"-Zeit beinahe erreicht ist. Trailing-Stop verwenden, um Gewinn zu maximieren.',
    'instructionsText2' => 'Beachten Sie bitte, dass verschiedene Makler haben unterschiedliche Quoten zur selben Zeit. Der Unterschied betragt in der Regel 5 Pips oder vielleicht mehr. Um dieses Problem zu beheben, versucht Livesignals.pro den durchschnittlichen Wert fur alle Angebote von verschiedenen Brokern zu berechnen und "durchschnittliche" Ergebnisse zu liefern. Dennoch ist es moglich, dass Ihr Handel den Einstiegs-/Take-Profit-/Stop-Loss-Level erreicht, wahrend Livesignals.pro das wegen der Unterschiede in Angeboten nicht tut und umgekehrt.',
    'disclaimer' => 'Haftungsablehnung',
    'disclaimerText1' => 'Aktien, Optionen, Binary Optionen hat Forex Handel und Zukunft großes Potenzial belohnt, sondern auch große potenzielle Gefahr. Sie müssen sich der Risiken bewusst und bereit sein, sie zu akzeptieren, um in die Lager-, Binär-Optionen oder Futures investieren. Handeln Sie nie mit Geld kann man sich nicht leisten, vor allem verlieren mit Leveraged Instrumente wie binäre Handel mit Optionen, Futures-Handel oder Devisenhandel. Diese Website ist weder eine Aufforderung noch ein Angebot zum Kauf / Verkauf Aktien, Futures oder Optionen. Keine Darstellung wird gemacht, dass jede Rechnung wird oder erleiden könnte, um Gewinne oder Verluste ähnlich denen auf dieser Website besprochen erreichen. Die vergangene Performance eines Handelssystems oder Methodik ist nicht notwendigerweise indikativ für künftige Ergebnisse. Sie könnten Ihr ganzes Geld verlieren zu schnell durch: schlechte Markt Geschäftsbedingungen, die mechanische Fehler, emotionale induzierten Fehlern, Nachrichten Überraschungen und Ergebnis veröffentlicht.',
    'disclaimerText2' => 'CFTC Richtlinie 4.41 – Hypothetische oder simulierte Leistungsergebnisse haben bestimmte Beschränkungen. Anders als reale Leistungsaufzeichnungen, repräsentieren simulierte Ergebnisse nicht das wirkliche Trading. Auch weil der Handel nicht ausgeführt worden ist, könnten die Ergebnisse in Bezug auf andere Marktfaktoren, wie z.B. „Mangel an Liquidität“, unter- oder überbewertet werden. Simulierte Trading Programme sind grundsätzlich auch abhängig von der Tatsache, dass sie entwickelt wurden, um nachträglich einen Profit daraus zu erzielen. In keiner Darstellung darf die Erwartung beschrieben sein, dass man damit vergleichbare Gewinne oder Verluste auf seinem Konto erzielen kann.',
    'disclaimerText3' => 'Die Eigentümer der Webseite und die Webseite verzichten hiermit auf alle Verpflichtungen infolge der Nutzung der Webseite und/oder der Information. Nutzung der Webseite, ihres Inhalts und der Information ist die alleinige Verantwortung des Users. Der User entlastet hiermit die Eigentümer der Webseite von aller Haftung für Schäden an seinem Computer, falls zutreffend, durch den Gebrauch der Webseite und/oder ihres Inhalts und/oder ihrer verschiedenen Dienstleistungen. Der User entlastet hiermit den Webseite-Eigentümer, die Webseite, ihre Partner, Agenten, Angestellten, Vorstands-mitglieder, Direktoren, Anteilseigner usw. von jeglicher Haftung für Verluste und/oder finanzielle Schäden und/oder persönliche körperliche Schäden und/oder Betrug als Folge der Verwendung der Webseite.',
    'disclaimerText4' => 'Alle Rechte vorbehalten. Mit der Nutzung dieser Webseite akzeptieren Sie unsere Nutzungsbedingungen.',
    'currentPerformance' => 'Aktuelle Ergebnisse',
    'all' => 'Alle',
    'Month-to-date' => 'Die letzten 30 Tage',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Kaufen',
    'sell' => 'Verkaufen',
    'filled' => 'Ausgeführt',
    'cancelled' => 'Abgebrochen',
    'from' => 'ab dem',
    'till' => 'bis zum',
    'soldAt' => 'Verkauft bei',
    'boughtAt' => 'Gekauft bei',
    'profitPips' => 'Gewinn, pips',
    'buyAt' => 'Kaufen bei',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Verkaufen bei',
    'subscribe' => 'Abonnieren Sie Signale',
    'subscribeText' => 'Abonnenten haben sofortigen Zugriff auf die Signale.',
    'Subscribe to signals' => 'Signale abonnieren',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => 'Sonderangebot',
    'months subscription' => 'Abonnement für Monate',
    'All pairs' => 'Alle Währungspaare',
    'Buy' => 'Kaufen',
    'Vat' => 'MwSt kann dem Betrag, den Sie zahlen müssen, angewandt werden. MwSt steht für Value Added Tax, und ist eine Steuer von der Regierung auf den Kaufpreis hinzugefügt.',
    'Your subscription is active' => 'Ihr Abonnement ist aktiv',
    'Expiration date' => 'Ablaufdatum',
    'Save' => 'Speichern',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => 'E-Mail-Adresse (eine reale Adresse)',
    'Password (6+ characters)' => 'Passwort (6+ Zeichen)',
    'Repeat your password' => 'Passwort Wiederholung',
    'Register a new account' => 'Neues Konto erstellen',
    'Please enter above characters' => 'Bitte geben Sie oben Zeichen',
    '' => '',
    'Please login' => 'Bitte einloggen',
    'Email' => 'E-Mail-Adresse',
    'Password' => 'Passwort',
    'Remember me?' => 'Erinnere dich an mich',
    'Forgot password ?' => 'Ich habe mein Passwort vergessen',
    ///////Auth END
    ///////Month
    'Date, time' => 'Datum, Zeit',
    'Order' => '',
    'Take Profit' => 'Take Profit',
    'Stop Loss' => 'Stop Loss',
    'State' => 'Status',
    'Entry' => 'Eröffnungspreis',
    'Exit' => 'Schlusspreis',
    'Profit' => 'Gewinn',
    ///////Month END
];
?>