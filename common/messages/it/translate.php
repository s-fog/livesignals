<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'Iscriviti',
    'My account' => 'Il mio conto',
    'Login' => 'Accedi',
    'Logout' => 'Uscire',
    'mainHeader' => 'Segnali Forex gratis',
    'topText' => '
        Livesignals.pro fornisce i 
        <strong>Segnali Forex online gratis</strong>
         con 
        <a href="#performance">risultati</a>
         in tempo reale per ogni 
         <a href="#totals">trading e per mese</a>.
        <br>
        Per rimanere informati, 
        <a href="'.Url::to(["site/index"]).'">aggiornare questa pagina</a>
        o 
        <a href="'.Url::to(["user/account"]).'">iscriversi via e-mail</a>
    ',
    'userOnline' => 'Utenti online',
    'instructions' => 'Istruzioni',
    'instructionsText1' => 'Bisogna mettere un ordine in attesa quando arriva il segnale (all’ora "dal"). L’ora "al" significa uscita forzata. Qualsiasi trade aperto viene "Eseguito" quando sta per arrivare l’ora "al". Qualsiasi ordine in attesa viene "Annullato" quando sta per arrivare l’ora "al". Usa il trailing stop per massimizzare i profitti.',
    'instructionsText2' => 'Si prega di tenere presente che broker differenti daranno quotazioni e prezzi differenti nello stesso momento. La differenza è di solito intorno ai 5 punti, probabilmente di più. Per risolvere questo problema Livesignals.pro cerca di fare una quotazione media tra quelle dei vari broker e fornisce i risultati "medi". Tuttavia è possibile che il vostro trade raggiunga il livello di entrata/take-profit/stop-loss e il trade Livesignals.pro no, oppure viceversa, a causa della differenza delle quotazioni.',
    'disclaimer' => 'Rinuncia di responsabilità',
    'disclaimerText1' => 'Azioni, opzioni, opzioni binarie, Forex trading e Futuro ha grandi potenziali ricompense, ma anche di grande rischio potenziale. È necessario essere consapevoli dei rischi ed essere disposti ad accettarli, al fine di investire nella stock option binario o mercati futures. Non effettuare scambi con denaro che non può permettersi di perdere soprattutto con strumenti leveraged come binario di scambio opzioni, futures o forex trading. Questo sito non è né una sollecitazione, né un\'offerta per l\'acquisto / vendita di azioni, futures e opzioni. Nessuna rappresentazione è stato fatto che qualsiasi account o sia idonea a realizzare profitti o le perdite simili a quelle discusse in questo sito. Il rendimento passato di un sistema di negoziazione o metodologia non è necessariamente indicativi di risultati futuri. Si potrebbe perdere tutti i vostri soldi in fretta a causa troppo: scarsa condizioni degli scambi di mercato, errore meccanico, emozionale errori indotti, sorprese notizie e comunicati guadagni.',
    'disclaimerText2' => 'REGOLA CFTC 4.41: RISULTATI IPOTETICI O SIMULATE PRESENTARE TALUNI LIMITI A DIFFERENZA DI UN RECORD EFFETTIVE PRESTAZIONI, RISULTATI SIMULATO NON RAPPRESENTANO TRADING REALE INOLTRE, POICHÉ LA CONTRATTI NON SONO STATI ESEGUITI, I RISULTATI POSSONO AVERE SOTTO -O-OVER COMPENSATA LA EVENTUALE IMPATTO, DEI FATTORI DI MERCATO QUALI AD ESEMPIO LA MANCANZA DI LIQUIDITÀ. PROGRAMMI DI TRADING SIMULATO IN GENERALE SONO ANCHE OGGETTO DEL FATTO CHE SI PROPONGONO CON IL SENNO DI POI. ALCUNA DICHIARAZIONE È STATO FATTO CHE OGNI CONTO VOLONTÀ O SIA IDONEA A CONSEGUIRE PROFITTI O PERDITE SIMILI A QUELLI MOSTRATI.',
    'disclaimerText3' => 'Il titolare del sito web rinuncia qualsiasi responsabilità riguarda l\'utilizzo del sito web e/o informazioni su esso. L\'utilizzo del sito web, i contenuti e le informazioni siano rese in materia di responsabilità esclusiva dell\'utente. L\'utente rilascia i proprietari del sito da qualsiasi responsabilità per danni causati al suo computer, per via dell\'uso del sito e/o del suo contenuto e/o dei suoi vari servizi. L\'utente rilascia i proprietari del sito web, il sito, i loro partner, agenti, impiegati, funzionari, dirigenti, direttori, azionisti, ecc da qualsiasi responsabilità per danni e/o danni finanziari e/o personali, danni fisici e/o di frodi , causati tramite l\'uso del sito.',
    'disclaimerText4' => 'Tutti i diritti riservati. L\'uso del sito presuppone l\'accettazione del contratto utente.',
    'currentPerformance' => 'Risultato corrente',
    'all' => 'TUTTI',
    'Month-to-date' => 'Ultimi 30 giorni',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Compra',
    'sell' => 'Vendi',
    'filled' => 'Eseguito',
    'cancelled' => 'Annullato',
    'from' => 'dal',
    'till' => 'al',
    'soldAt' => 'Venduto a',
    'boughtAt' => 'Comprato a',
    'profitPips' => 'Profitto, punto(i)',
    'buyAt' => 'Acquista a',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Vendi a',
    'subscribe' => 'Iscriviti ai segnali',
    'subscribeText' => 'Gli abbonati hanno accesso immediato ai segnali.',
    'Subscribe to signals' => 'Iscrizione ai segnali',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => 'Offerta speciale',
    'months subscription' => 'mesi di iscrizione',
    'All pairs' => 'Tutte le coppie',
    'Buy' => 'Acquista',
    'Vat' => 'IVA può essere applicato per l\'importo che si deve pagare. IVA acronimo di imposta sul valore aggiunto, ed è una imposta sul valore aggiunto da parte del governo al prezzo di acquisto.',
    'Your subscription is active' => 'Il tuo abbonamento è attivo',
    'Expiration date' => 'Data di scadenza',
    'Save' => 'Salviamo',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => 'Indirizzo email (un indirizzo reale)',
    'Password (6+ characters)' => 'Password (6 + caratteri)',
    'Repeat your password' => 'Ripetere password',
    'Register a new account' => 'Registra un nuovo conto',
    'Please enter above characters' => 'Immetti i caratteri che vedi',
    '' => '',
    'Please login' => 'Accedi',
    'Email' => '',
    'Password' => '',
    'Remember me?' => 'Mantieni l\'accesso',
    'Forgot password ?' => 'Ho dimenticato la password',
    ///////Auth END
    ///////Month
    'Date, time' => 'Data, ora',
    'Order' => 'Ordine',
    'Take Profit' => '',
    'Stop Loss' => '',
    'State' => 'Stato',
    'Entry' => 'Entrata',
    'Exit' => 'Uscita',
    'Profit' => 'Profitto',
    ///////Month END
];
?>