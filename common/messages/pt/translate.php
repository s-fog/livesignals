<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'Inscrever',
    'My account' => 'A minha conta',
    'Login' => 'Iniciar sessão',
    'Logout' => 'Terminar sessão',
    'mainHeader' => 'Sinais Forex grátis',
    'topText' => '
        Livesignals.pro fornece 
        <strong>Sinais Forex Gratuitos online</strong>, 
         com 
        <a href="#performance">relatório</a>
         de 
         <a href="#totals">desempenho</a> 
         e em tempo-real. 
        <br>
        <a href="'.Url::to(["site/index"]).'">Actualize esta página</a>
         para se manter informado ou 
        <a href="'.Url::to(["user/account"]).'">subscreva por e-mail</a>.
    ',
    'userOnline' => 'Usuários online',
    'instructions' => 'Instruções',
    'instructionsText1' => 'A ordem pendente deverá ser colocada assim que chega o sinal (no parâmetro "De"). O parâmetro "Até" foi desenhado para forçar a saída. Qualquer negociação aberta tem que ser preenchida até ao final do "Até". Quando o "Até" termina, a ordem será cancelada. Use trailing-stop para maximizar o lucro.',
    'instructionsText2' => 'Observe que corretores diferentes lhe fornecerão cotações e preços diferentes em momentos específicos. A diferença é por vezes de 5 pips ou mais. Para ultrapassar este problema o Livesignals.pro tenta fazer a média entre várias cotações de diferentes correctoras e dar um resultado médio. Contudo, é possível que a negociação atinja parâmetros de entrada/redenção de lucros/travar-queda enquanto o Livesignals.pro ainda não o fez e vice-versa, devido às diferenças nas cotações.',
    'disclaimer' => 'Disclaimer',
    'disclaimerText1' => 'Ações, Opções, opções de binário, Forex e Futuro tem grandes recompensas potenciais, mas também grandes riscos potenciais. Você deve estar ciente dos riscos e estar disposto a aceitá-las, a fim de investir em ações, opções de binário ou mercados de futuros. Não comércio com dinheiro que você não pode perder, especialmente com os instrumentos alavancados, como o comércio opções binárias, negociação de futuros ou forex. Este site não é nem uma solicitação nem uma oferta para comprar / vender acções, futuros ou opções. Nenhuma respresentação está sendo feita que todo o cliente ou é provável conseguir os lucros ou as perdas similares àqueles discutida neste site. O desempenho passado de nenhuma sistema de comércio ou metodologia não é necessariamente indicativo dos resultados futuros. Você pode perder todo seu dinheiro rápido devido também: condições ruins de mercado de negociação, erro mecânico, emocional erros induzidos, surpresas notícias e press releases.',
    'disclaimerText2' => 'REGRA CFTC 4,41: RESULTADOS DE DESEMPENHO HIPOTÉTICOS OU SIMULADOS ALGUMAS LIMITAÇÕES AO CONTRÁRIO DE UM REGISTRO DE DESEMPENHO, RESULTADOS SIMULADOS NÃO REPRESENTAM A TROCA REAL TAMBÉM, DESDE O COMÉRCIOS NÃO FORAM EXECUTADOS, OS RESULTADOS PODEM TER EM OR-OVER COMPENSADOS DO IMPACTO, EVENTUALMENTE, DE FATORES DE MERCADO, TAIS COMO A FALTA DE LIQUIDEZ. PROGRAMAS DE TROCA SIMULADOS EM GERAL TAMBÉM SÃO SUJEITOS AO FATO DE QUE FORAM CRIADOS COM O BENEFÍCIO DE APRENDIZAGEM. REPRESENTAÇÃO NO QUE ESTÁ SENDO FEITA TODA A VONTADE OU É PROVÁVEL QUE CONSEGUIR O LUCRO OU PREJUÍZOS SEMELHANTES AOS APRESENTADOS.',
    'disclaimerText3' => 'Os proprietários do site e o site, por meio deste, renunciam a qualquer responsabilidade devido ao uso do site e/ou informações. O uso do site, o conteúdo e as informações são de responsabilidade exclusiva do usuário. O usuário, por meio deste, isenta os proprietários do site de qualquer responsabilidade por danos causados ao seu computador, através do uso do site e/ou seu conteúdo e/ou seus vários serviços. O usuário, por meio deste, isenta os proprietários do site, o site, seus sócios, agentes, funcionários, diretores, gerentes, administradores, acionistas, etc. de qualquer responsabilidade por perdas e/ou prejuízos financeiros e/ou danos físicos pessoais e/ou fraude, causada pelo uso do site.',
    'disclaimerText4' => 'Todos os direitos reservados. O uso deste website constitui a aceitação do nosso contrato do usuário.',
    'currentPerformance' => 'Desempenho atual',
    'all' => 'TODOS',
    'Month-to-date' => 'Mensal até hoje',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Compra',
    'sell' => 'Venda',
    'filled' => 'Completo',
    'cancelled' => 'Cancelado',
    'from' => 'De',
    'till' => 'até',
    'soldAt' => 'Vendido em',
    'boughtAt' => 'Comprado em',
    'profitPips' => 'Lucro, pontos',
    'buyAt' => 'Comprar em',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Vende em',
    'subscribe' => 'Inscrever-se para sinais',
    'subscribeText' => 'Assinantes têm acesso instantâneo aos sinais.',
    'Subscribe to signals' => 'Assinar sinais',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => 'Oferta especial',
    'months subscription' => 'Assinatura de meses',
    'All pairs' => 'Todos os pares',
    'Buy' => 'Comprar',
    'Vat' => 'IVA pode ser aplicada para a quantidade que você tem que pagar. IVA significa Imposto sobre Valor Agregado, e é um imposto adicionado pelo governo para o preço de compra.',
    'Your subscription is active' => 'Sua assinatura está ativa.',
    'Expiration date' => 'Data de validade',
    'Save' => 'Guardar',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => 'Endereço eletrônico (endereço real)',
    'Password (6+ characters)' => 'Senha (6+ caracteres)',
    'Repeat your password' => 'Repetir a senha',
    'Register a new account' => 'Crie sua Conta',
    'Please enter above characters' => 'Introduza os caracteres acima',
    '' => '',
    'Please login' => 'Por favor, faça o login',
    'Email' => '',
    'Password' => 'Senha',
    'Remember me?' => 'Lembrar de mim',
    'Forgot password ?' => 'Esqueci minha senha',
    ///////Auth END
    ///////Month
    'Date, time' => 'Data e hora',
    'Order' => 'Pedido',
    'Take Profit' => '',
    'Stop Loss' => '',
    'State' => 'Estado',
    'Entry' => 'Entrada',
    'Exit' => 'Saída',
    'Profit' => 'Lucro',
    ///////Month END
];
?>