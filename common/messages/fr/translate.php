<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'S\'inscrire',
    'My account' => 'Mon compte',
    'Login' => 'Connexion',
    'Logout' => 'Déconnexion',
    'mainHeader' => 'Signaux Forex gratuits',
    'topText' => '
        Livesignals.pro fournit des 
        <strong>signaux de Forex gratuits</strong>
         en ligne avec des 
        <a href="#performance">performances</a>
         en temps reel et des 
         <a href="#totals">totaux</a>.
        <br>
        Pour rester informe 
        <a href="'.Url::to(["site/index"]).'">rafraichissez cette page</a>
        ou  
        <a href="'.Url::to(["user/account"]).'">abonnez-vous par courriel</a>
    ',
    'userOnline' => 'Utilisateurs en ligne',
    'instructions' => 'Instructions',
    'instructionsText1' => 'L\'ordre en attente doit être placé quand le signal arrive (au moment «Du»). Le moment « Jusqu\'àu» est destiné à la sortie forcée. Toute ouverture d\'opération de change est «Exécuté» quand le moment «Jusqu\'àu» est sur le point d\'être atteint. Tout ordre en attente est «Annulé» lorsque le moment «Jusqu\'à» est sur le point d\'être atteint. Utiliser un ordre de stop suiveur pour maximiser les profits.',
    'instructionsText2' => 'Veuillez garder à l\'esprit que différents courtiers vous donneront des cotations et des prix différents à un moment précis. La différence est habituellement d\'environ 5 pips et peut-être plus. Pour surmonter ce problème Livesignals.pro essaie de faire une moyenne des quotités de différents courtiers et fournit une «moyenne» des résultats. Néanmoins, il est possible que votre opération de change atteigne l\'entrée/le take-profit/le niveau de stop-loss, lorsque l\'opération de change Livesignals.pro ne fonctionne pas et vice versa, en raison d\'une différence de quotité.',
    'disclaimer' => 'Avis de non-responsabilité',
    'disclaimerText1' => 'Actions, options, options binaires, Forex et l\'avenir a de grandes retombées potentielles, mais aussi des grands risques potentiels. Vous devez être conscients des risques et être prêts à les accepter en vue d\'investir dans les actions, les options binaires ou les marchés à terme. Ne pas le commerce avec l\'argent vous ne pouvez pas se permettre de perdre en particulier avec les instruments à effet de levier comme les opérations sur options binaires, les contrats à terme ou de trading forex. Ce site n\'est ni une sollicitation ni une offre d\'achat / vente d\'actions, contrats à terme ou d\'options. Aucune représentation n\'est faite que tout compte ou est susceptible de réaliser des gains ou des pertes semblables à ceux qui sont développés sur ce site. La performance passée d\'un système commercial ou de la méthodologie n\'est pas nécessairement indicatifs des résultats futurs. Vous pourriez perdre tout votre argent rapidement en raison aussi: les mauvaises conditions de négociation sur le marché, d\'une erreur mécanique, émotionnelle erreurs induites, surprises nouvelles et de publication des résultats.',
    'disclaimerText2' => 'LA RÈGLE CFTC 4.41. RÉSULTATS HYPOTHÉTIQUES D\'EXÉCUTION OU SIMULÉES ONT CERTAINES LIMITES DIFFERENCE D\'UN RECORD DU RENDEMENT RÉEL, RÉSULTATS SIMULÉS NE REPRESENTE PAS LE COMMERCE RÉEL AUSSI, DEPUIS LE COMMERCES N\'ONT PAS ÉTÉ SIGNÉ, LES RÉSULTATS PEUVENT AVOIR EN VERTU -OU-PLUS POUR LA COMPENSATION IMPACT ÉVENTUEL, DES FACTEURS DU MARCHÉ CERTAINS, COMME LE MANQUE DE LIQUIDITÉ. PROGRAMMES DE NÉGOCIATION SIMULE EN GÉNÉRAL SONT ÉGALEMENT SOUMIS AUX FAIT QU\'ILS SONT DESTINEES A L\'AVANTAGE DU RECUL. AUCUNE DÉCLARATION EST FAITE QUE TOUT COMPTE VA OU EST SUSCEPTIBLE DE RÉALISER LE BÉNÉFICE OU PERTE SEMBLABLES A CELLES MONTRÉ.',
    'disclaimerText3' => 'Les propriétaires du site Web et le site Web rejettent toute responsabilité, quelle qu\'elle soit, de l\'utilisation du site Web et/ou des informations qui y sont affichées. L\'utilisation du site Web, de son contenu et des informations qui y sont affichés sont conçus à la seule responsabilité de l\'utilisateur. L\'utilisateur dégage les propriétaires du site Web de toute responsabilité de dommage causé à son ordinateur, à travers l\'utilisation du site Web et/ou son contenu et/ou ses divers services. L\'utilisateur dégage, par le présent accord, les propriétaires du site Web, leurs partenaires, agents, employés, directeurs, managers, actionnaires, etc. de toute responsabilité de pertes et/ou dommages financiers et/ou dommages corporels, personnels et/ou fraudes, causés suite à l\'utilisation du site.',
    'disclaimerText4' => 'Tous droits réservés. L\'utilisation de ce site web implique l\'acceptation de notre accord d\'utilisateur.',
    'currentPerformance' => 'Résultat actuel',
    'all' => 'TOUS',
    'Month-to-date' => 'Cumul mensuel au',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Achat',
    'sell' => 'Vente',
    'filled' => 'Exécuté',
    'cancelled' => 'Annulé',
    'from' => 'du',
    'till' => 'jusqu\'au',
    'soldAt' => 'En vente pour',
    'boughtAt' => 'Acheté pour',
    'profitPips' => 'Bénéfice, pips',
    'buyAt' => 'Acheter pour',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Vendre pour',
    'subscribe' => 'Abonnez-vous à des signaux',
    'subscribeText' => 'Les abonnés ont un accès instantané à des signaux.',
    'Subscribe to signals' => 'S\'inscrire aux signaux',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => 'Offre spéciale',
    'months subscription' => 'Inscription pour mois',
    'All pairs' => 'Toutes les paires',
    'Buy' => 'Acheter',
    'Vat' => 'Le prix d\'achat est réputé inclure la taxe sur la valeur ajoutée qui aurait été due si la marge bénéficiaire imposable réalisée par le fournisseur avait été égale à 20 % du prix d\'achat.',
    'Your subscription is active' => 'Votre abonnement est activé.',
    'Expiration date' => 'Date d\'expiration',
    'Save' => 'Sauvegarder',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => 'Adresse e-mail (une adresse réelle)',
    'Password (6+ characters)' => 'Mot de passe (6  caractères)',
    'Repeat your password' => 'Répétez le mot de passe',
    'Register a new account' => 'Créer un nouveau compte',
    'Please enter above characters' => 'Veuillez recopier le code ci-dessus',
    '' => '',
    'Please login' => 'Veuillez vous connecter',
    'Email' => '',
    'Password' => 'Mot de passe',
    'Remember me?' => 'Se souvenir de moi pendant',
    'Forgot password ?' => 'J’ai oublié mon mot de passe',
    ///////Auth END
    ///////Month
    'Date, time' => 'Date, heure',
    'Order' => 'Ordre',
    'Take Profit' => '',
    'Stop Loss' => '',
    'State' => 'Statut',
    'Entry' => 'Entrée',
    'Exit' => 'Sortie',
    'Profit' => 'Bénéfice',
    ///////Month END
];
?>