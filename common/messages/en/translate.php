<?php
use yii\helpers\Url;

return [
    ///////Главная
    'Register' => 'Register',
    'My account' => 'My account',
    'Login' => 'Login',
    'Logout' => 'Logout',
    'mainHeader' => 'Free Forex Signals',
    'topText' => '
        Livesignals.pro provides 
        <strong>Free Forex Signals online</strong>
         with realtime 
        <a href="#performance">performance</a>
         and 
         <a href="#totals">totals</a>.
        <br>
        To stay informed 
        <a href="'.Url::to(["site/index"]).'">refresh this page</a>
        or 
        <a href="'.Url::to(["user/account"]).'">subscribe by e-mail</a>
    ',
    'userOnline' => 'Users online',
    'instructions' => 'Instructions',
    'instructionsText1' => 'Pending order should be placed as signal arrives (at "From" time). "Till" time is intended to forced exit. Any open trade is "Filled" when "Till" time is about to be reached. Any pending order is "Cancelled" when "Till" time is about to be reached. Use trailing-stop to maximize profit.',
    'instructionsText2' => 'Please keep in mind that different brokers give different quotes at a specific point of time. The difference is usually about 5 pips and perhaps more. To overcome this issue Livesignals.pro tries to average quotes from different brokers and provides "average" results. Nevertheless it\'s possible that your trade reaches entry/take-profit/stop-loss level when Livesignals.pro trade doesn\'t and vice versa due to quote difference.',
    'disclaimer' => 'Disclaimer',
    'disclaimerText1' => 'Stocks, Options, Binary options, Forex and Future trading has large potential rewards, but also large potential risk. You must be aware of the risks and be willing to accept them in order to invest in the stock, binary options or futures markets. Don\'t trade with money you can\'t afford to lose especially with leveraged instruments such as binary options trading, futures trading or forex trading. This website is neither a solicitation nor an offer to Buy/Sell stocks, futures or options. No representation is being made that any account will or is likely to achieve profits or losses similar to those discussed on this website. The past performance of any trading system or methodology is not necessarily indicative of future results. You could lose all of your money fast due too: poor market trading conditions, mechanical error, emotional induced errors, news surprises and earnings releases.',
    'disclaimerText2' => 'CFTC RULE 4.41 - HYPOTHETICAL OR SIMULATED PERFORMANCE RESULTS HAVE CERTAIN LIMITATIONS. UNLIKE AN ACTUAL PERFORMANCE RECORD, SIMULATED RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, SINCE THE TRADES HAVE NOT BEEN EXECUTED, THE RESULTS MAY HAVE UNDER-OR-OVER COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED TRADING PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFIT OR LOSSES SIMILAR TO THOSE SHOWN.',
    'disclaimerText3' => 'The owners of the website and the website hereby waive any liability whatsoever due to the use of the website and/or information. Use of the website, the content and the information is made on the user\'s sole liability. The user hereby releases the owners of the website from any liability for damage caused to his computer, in any, through the use of the website and/or its content and/or its various services. The user hereby releases the owners of the website, the website, their partners, agents, employees, officers, managers, directors, shareholders, etc. from any liability for losses and/or financial damages and/or personal bodily damages and/or fraud, caused through the use of the website.',
    'disclaimerText4' => 'All rights reserved. The use of this website constitutes acceptance of our user agreement.',
    'currentPerformance' => 'Current performance',
    'all' => 'ALL',
    'Month-to-date' => 'Month-to-date',
    ///////Главная END
    ///////Сигнал
    'signal' => 'signal',
    'buy' => 'Buy',
    'sell' => 'Sell',
    'filled' => 'Filled',
    'cancelled' => 'Cancelled',
    'from' => 'From',
    'till' => 'Till',
    'soldAt' => 'Sold at',
    'boughtAt' => 'Bought at',
    'profitPips' => 'Profit, pips',
    'buyAt' => 'Buy at',
    'takeProfitAt' => 'Take profit* at',
    'stopLossAt' => 'Stop loss at',
    'sellAt' => 'Sell at',
    'subscribe' => 'Subscribe to signals',
    'subscribeText' => 'Subscribers have instant access to the signals.',
    ///////Сигнал END
    ///////Аккаунт
    'Special offer' => '',
    'months subscription' => '',
    'All pairs' => '',
    'Buy' => '',
    'Vat' => 'VAT may be applied to the amount you have to pay. VAT stands for Value Added Tax, and is a tax added by the government to the purchase price.',
    'Your subscription is active' => '',
    'Expiration date' => '',
    ///////Аккаунт END
    ///////Auth
    'Email address (a real address)' => '',
    'Password (6+ characters)' => '',
    'Repeat your password' => '',
    'Register a new account' => '',
    'Please enter above characters' => '',
    'Please login' => '',
    'Email' => '',
    'Password' => '',
    'Remember me?' => '',
    'Log in' => '',
    'Registration' => '',
    'Forgot password ?' => '',
    '' => '',
    ///////Auth END
];
?>