<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Signal $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="signal-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Signal',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            

<!-- attribute category_id -->
			<?= $form->field($model, 'category_id')->textInput() ?>

<!-- attribute from -->
			<?= $form->field($model, 'from')->textInput(['maxlength' => true]) ?>

<!-- attribute till -->
			<?= $form->field($model, 'till')->textInput(['maxlength' => true]) ?>

<!-- attribute state -->
			<?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

<!-- attribute byu_at -->
			<?= $form->field($model, 'byu_at')->textInput(['maxlength' => true]) ?>

<!-- attribute take_profit_at -->
			<?= $form->field($model, 'take_profit_at')->textInput(['maxlength' => true]) ?>

<!-- attribute stop_loss_at -->
			<?= $form->field($model, 'stop_loss_at')->textInput(['maxlength' => true]) ?>

<!-- attribute sell_at -->
			<?= $form->field($model, 'sell_at')->textInput(['maxlength' => true]) ?>

<!-- attribute profit_pips -->
			<?= $form->field($model, 'profit_pips')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Signal'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Create' : 'Save'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

