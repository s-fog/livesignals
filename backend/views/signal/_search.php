<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\SignalSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="signal-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'category_id') ?>

		<?= $form->field($model, 'from') ?>

		<?= $form->field($model, 'till') ?>

		<?= $form->field($model, 'state') ?>

		<?php // echo $form->field($model, 'byu_at') ?>

		<?php // echo $form->field($model, 'take_profit_at') ?>

		<?php // echo $form->field($model, 'stop_loss_at') ?>

		<?php // echo $form->field($model, 'sell_at') ?>

		<?php // echo $form->field($model, 'profit_pips') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
