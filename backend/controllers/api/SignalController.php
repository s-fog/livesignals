<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "SignalController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SignalController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Signal';
}
