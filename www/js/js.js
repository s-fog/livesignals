$(document).ready(function() {
    $('.js-language').on('click', function() {
        $('.js-language-dropdown').toggle();
        return false;
    });

    $('.captchaRefresh').click(function() {
        $('#registrationform-captcha-image').click();
    });
});
$(window).load(function() {
    $('.payment [type="submit"]').click(function(event) {
        event.preventDefault();

        yaCounter46314360.reachGoal('before_order');
        ga('send', 'event', 'ls', 'before_order');
        fbq('track', 'Lead');

        setTimeout(function() {
            $(event.target).parents('.payment').submit();
        }, 500);
    });
});